# T2PKycSDK Example

# How to use T2PKycSDK?

## Example

To run the example project, clone the repo, and run `install` from the Example directory first.


## Requirements

- Minimun Android API level 24 (minSdkVersion 24)


## Installation

Include the repositories in your level project 'build.gradle'
```groovy
...
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.t2p.co.th/api/v4/projects/236/packages/maven"
            credentials(HttpHeaderCredentials) {
                name = 'Private-Token'
                value = T2P_KYC_SDK_ACCESS_TOKEN
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
..
```

Include dependencies in your level app 'build.gradle'
```groovy
...
dependencies {
    ...
    implementation 'com.t2pco.t2pekycsdk:t2pekycsdk:1.1.33'
    ...
}
...
```

## ProGuard

If you are using Proguard, You  must include the code below in proguard-rules.pro file.
```groovy
...
-keep class net.sf.scuba.smartcards.IsoDepCardService {*;}
-keep class org.jmrtd.** { *; }
-keep class net.sf.scuba.** {*;}
-keep class org.bouncycastle.** {*;}
-keep class org.spongycastle.** {*;}
-keep class org.ejbca.** {*;}
...
```

## Usage

In Activity or Fragment

kotlin :
```kotlin
...
val config = T2PKycSdkConfig().apply {
    kycUserRef = "" // kycUserRef is refer to user
    kycToken = "" // kycToken is key for access SDK
    environment = "test" // For test server, default is production server
    language = "en" // support Thai and English language (th|en)
    fcmToken = "fcm_token" // your firebase cloud messaging token
}

T2PKycSdk.initializeSdk(config).startSdk(context){ it ->
    if(it.meta.responseCode != 600){
        // Error
        return@startSdk
    }

    // Success
    // You get user information form T2PKycSdkResponse.Data class
    Log.d("T2PKycSdkResponse.Data", "${it.data}")
    // You can send kycServiceCode and kycSessionCode to your server
    // for check or retrieve user information from API SDK Server
}   	
...
```

java :
```kotlin
...
T2PKycSdkConfig config = new T2PKycSdkConfig();
config.setKycUserRef(""); // kycUserRef is refer to user
config.setKycToken(""); // kycToken is key for access SDK
config.setEnvironment("test"); // For test server, default is production server
config.setLanguage("en"); // support Thai and English language (th|en)
config.setFcmToken("fcm_token"); // your firebase cloud messaging token

T2PKycSdk.INSTANCE.initializeSdk(config).startSdk(context, new Function1<T2PKycSdkResponse, Unit>() {
   @Override
   public Unit invoke(T2PKycSdkResponse response) {
    if(response.getMeta().getResponseCode() != 600){
     // Error
     return null;
    }

    // Success
    // You get user information form T2PKycSdkResponse.Data class
    Log.d("T2PKycSdkResponse.Data", response.getData().toString());
    // You can send kycServiceCode and kycSessionCode to your server
    // for check or retrieve user information from API SDK Server
    return null;
   }
}); 	
...
```


## Author

Chettha, chettha_pil@t2pco.com