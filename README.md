# ตัวอย่างการใช้งาน T2PKycSDK

## ข้อกำหนด

- แอนดรอย API ตั้งแต่ 24 ขึ้นไป (minSdkVersion 24)


## การติดตั้ง

เพิ่ม repositories ในระดับโปรเจคในไฟล์ 'build.gradle'
```groovy
...
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.t2p.co.th/api/v4/projects/236/packages/maven"
            credentials(HttpHeaderCredentials) {
                name = 'Private-Token'
                value = T2P_KYC_SDK_ACCESS_TOKEN
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
..
```

เพิ่ม dependencies ในระดับแอพฯในไฟล์  'build.gradle'
```groovy
...
dependencies {
    ...
    implementation 'com.t2pco.t2pekycsdk:t2pekycsdk:1.1.33'
    ...
}
...
```

## ProGuard

หากมีการเปิดใช้ ProGuard จำเป็นต้องเพิ่มโค้ดข้างล่างในไฟล์ proguard-rules.pro
```groovy
...
-keep class net.sf.scuba.smartcards.IsoDepCardService {*;}
-keep class org.jmrtd.** { *; }
-keep class net.sf.scuba.** {*;}
-keep class org.bouncycastle.** {*;}
-keep class org.spongycastle.** {*;}
-keep class org.ejbca.** {*;}
...
```

## การเรียกใช้งาน

ใน Activity หรือ Fragment

kotlin :
```kotlin
...
val config = T2PKycSdkConfig().apply {
    kycUserRef = "" // kycUserRef คือค่าที่เอาไว้อ้างอิงถึงตัวผู้ใช้
    kycToken = "" // kycToken คือค่าที่เอาไว้เข้าถึง SDK
    environment = "test" // สำหรับเลือกใช้งานบนเซิร์ฟเวอร์ทดสอบ, ค่าเริ่มต้นจะเป็นเซิร์ฟเวอร์ใช้งานจริง
}

T2PKycSdk.initializeSdk(config).startSdk(context){ it ->
    if(it.meta.responseCode != 600){
        // พบข้อผิดพลาด
        return@startSdk
    }

    // สำเร็จ
    // คุณสามารถดึงข้อมูลต่างๆจากคลาส T2PKycSdkResponse.Data
    Log.d("T2PKycSdkResponse.Data", "${it.data}")
}
...
```

java :
```kotlin
...
T2PKycSdkConfig config = new T2PKycSdkConfig();
config.setKycUserRef(""); // kycUserRef is refer to user
config.setKycToken(""); // kycToken is key for access SDK
config.setEnvironment("test"); // For test server, default is production server

T2PKycSdk.INSTANCE.initializeSdk(config).startSdk(context, new Function1<T2PKycSdkResponse, Unit>() {
   @Override
   public Unit invoke(T2PKycSdkResponse response) {
    if(response.getMeta().getResponseCode() != 600){
     // Error
     return null;
    }

    // Success
    // You get user information form T2PKycSdkResponse.Data class
    Log.d("T2PKycSdkResponse.Data", response.getData().toString());
    // You can send kycServiceCode and kycSessionCode to your server
    // for check or retrieve user information from API SDK Server
    return null;
   }
}); 	
...
```

## การกำหนดค่าแบบต่างๆ

กำหนดค่าแบบเริ่มต้น
```kotlin
...
val config = T2PKycSdkConfig().apply {
    kycUserRef = "" // kycUserRef คือค่าที่เอาไว้อ้างอิงถึงตัวผู้ใช้
    kycToken = "" // kycToken คือค่าที่เอาไว้เข้าถึง SDK
    environment = "test" // สำหรับเลือกใช้งานบนเซิร์ฟเวอร์ทดสอบ, ค่าเริ่มต้นจะเป็นเซิร์ฟเวอร์ใช้งานจริง
}
...
```

กำหนดค่าเพิ่มเติม กรณีรับรองการยืนยันตัวตนด้วยการ Dip-Chip
```kotlin
...
val config = T2PKycSdkConfig().apply {
    ...
    fcmToken = "xxx" // fcmToken คือค่า token จาก Firebase Cloud Messaging. ใช้สำหรับการยืนยันตัวตนด้วย Dip-Chip
}
...
```

กำหนดค่าเพิ่มเติม กรณีผู้ใช้ยืนยันตัวตนด้วยบัตรประชาชนแบบถ่ายรูปบัตรแล้ว และต้องการให้ผู้ใช้ยันตัวตนด้วยการ Dip-Chip เพิ่ม
```kotlin
...
val config = T2PKycSdkConfig().apply {
    ...
    fcmToken = "xxx" // fcmToken คือค่า token จาก Firebase Cloud Messaging. ใช้สำหรับการยืนยันตัวตนด้วย Dip-Chip
    kycSessionCode = "xxx" // kycSessionCode คือค่าที่ได้จากการยืนยันตัวตนด้วยบัตรประชาชนแบบถ่ายรูปบัตร
    kycServiceCode = "xxx" // kycServiceCode คือค่าที่ได้จากการยืนยันตัวตนด้วยบัตรประชาชนแบบถ่ายรูปบัตร
    kycType = T2PKycType.KIOSK_SCANNER // kycType คือค่าต้องการให้ SDK เริ่มต้นให้แสดงหน้าไหน , ค่าเริ่มต้นคือ T2PKycType.DOCUMENT_LIST
}
...
```

## หมายเหตุ

ถ้า config มีการใช้ fcmToken จำเป็นต้องเรียกฟังก์ชัน T2PKycSdk.onFCMReceived(context, data) ที่ onMessageReceived
ในตอนที่แอพฯได้รับแจ้งเตือนจาก Firebase Cloud Messaging
```kotlin
...
class MyFirebaseMessagingService : FirebaseMessagingService(){

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        T2PKycSdk.onFCMReceived(this, remoteMessage.data)
    }
...
```

## ผู้เขียน

Chettha, chettha_pil@t2pco.com

