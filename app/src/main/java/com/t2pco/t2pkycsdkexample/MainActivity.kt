package com.t2pco.t2pkycsdkexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.messaging.FirebaseMessaging
import com.t2pco.t2pekycsdk.v3.T2PKycSdk
import com.t2pco.t2pekycsdk.v3.services.models.T2PKycSdkConfig
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigInteger
import java.security.MessageDigest

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvVersion.text = "Version ${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})"
        etUserRef.setText("0800000000")
        etAuthorizationToken.setText("ZXlKamJHbGxiblJEYjJSbElqb2lWREpRUkVWTlR5SXNJbU5zYVdWdWRFeHBZbFpsY25OcGIyNGlPaUl4TGpBdU1DSXNJbXRsZVVOdlpHVWlPaUpFUlUxUExUQXdNREVpTENKdFpYUm9iMlFpT2lKUVQxTlVJaXdpZEdsdFpYTjBZVzF3SWpvaU1qQXlNakF5TVRVeE5qTTBNamNpTENKMGIydGxibFI1Y0dVaU9pSkRJaXdpZFhKcElqb2lJbjA9OjljOTdkYjIyZDQ5ZmZhNzFiYTI5ZGRiM2Y3MTFjMzIzNGI4OWNjODJmYTM0MGNjOTk3ZWRlMTdhNDQxMzBiYjE4ZTU2OTA1YzQwMDkzMzIxYmZiMTg5YzJmMDMwYmUyZGU4ZTAzNDczMTA0ZDUyNzViMTA0ZWI1MDA4MDg5NjFh")
        btStartKyc.setOnClickListener {
            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    startSDK(task.result)
                } else {
                    startSDK("xxx")
                }
            }.addOnCanceledListener {
                startSDK("xxx")
            }
        }
    }

    private fun startSDK(fcmToken: String?) {
        val env = when(rdEnvironment.checkedRadioButtonId){
            R.id.rdDev -> "dev"
            R.id.rdTest -> "test"
            else -> "prod"
        }

        val lang = when(rdLanguage.checkedRadioButtonId){
            R.id.rdTH -> "th"
            else -> "en"
        }

        val config = T2PKycSdkConfig().apply {
            kycUserRef = md5(etUserRef.text.toString()) // kycUserRef is refer to user
            kycToken = etAuthorizationToken.text.toString() // kycToken is key for access SDK
            environment = env // For test server, default is production
            language = lang
            this.fcmToken = fcmToken?: ""
        }

        T2PKycSdk.initializeSdk(config).startSdk(this){
            if(it.meta.responseCode != 600){
                // Error
                Toast.makeText(
                    applicationContext,
                    "(${it.meta.responseCode})${it.meta.responseMessage}",
                    Toast.LENGTH_SHORT
                ).show()
                return@startSdk
            }

            // Success
            Toast.makeText(
                applicationContext,
                "Success",
                Toast.LENGTH_SHORT
            ).show()

            // You get user information form T2PKycSdkResponse.Data class
            // Log.d("T2PKycSdkResponse.Data", "${it.data}")
            // You can send kycServiceCode and kycSessionCode to your server
            // for check or retrieve user information from API SDK Server
        }
    }

    private fun md5(input:String): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }
}
