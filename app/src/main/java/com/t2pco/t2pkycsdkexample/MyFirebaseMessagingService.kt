package com.t2pco.t2pkycsdkexample

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.t2pco.t2pekycsdk.v3.T2PKycSdk

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        T2PKycSdk.onFCMReceived(this, message.data);
    }

}