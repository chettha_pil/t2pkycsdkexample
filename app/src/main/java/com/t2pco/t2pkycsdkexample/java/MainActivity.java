package com.t2pco.t2pkycsdkexample.java;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.t2pco.t2pekycsdk.v3.T2PKycSdk;
import com.t2pco.t2pekycsdk.v3.services.models.T2PKycSdkConfig;
import com.t2pco.t2pekycsdk.v3.services.models.response.T2PKycSdkResponse;
import com.t2pco.t2pkycsdkexample.BuildConfig;
import com.t2pco.t2pkycsdkexample.R;
import org.jetbrains.annotations.NotNull;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class MainActivity extends AppCompatActivity {
    TextView tvVersion;
    EditText etUserRef;
    EditText etAuthorizationToken;
    RadioGroup rdEnvironment;
    RadioGroup rdLanguage;
    Button btStartKyc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvVersion = findViewById(R.id.tvVersion);
        etUserRef = findViewById(R.id.etUserRef);
        etAuthorizationToken = findViewById(R.id.etAuthorizationToken);
        rdEnvironment = findViewById(R.id.rdEnvironment);
        rdLanguage = findViewById(R.id.rdLanguage);
        btStartKyc = findViewById(R.id.btStartKyc);

        tvVersion.setText("Version " + BuildConfig.VERSION_NAME + BuildConfig.VERSION_CODE);
        etUserRef.setText("0800000000");
        etAuthorizationToken.setText("ZXlKamJHbGxiblJEYjJSbElqb2lWREpRUkVWTlR5SXNJbU5zYVdWdWRFeHBZbFpsY25OcGIyNGlPaUl4TGpBdU1DSXNJbXRsZVVOdlpHVWlPaUpFUlUxUExUQXdNREVpTENKdFpYUm9iMlFpT2lKUVQxTlVJaXdpZEdsdFpYTjBZVzF3SWpvaU1qQXlNVEE0TWpNeE9ETXhNRElpTENKMGIydGxibFI1Y0dVaU9pSkRJaXdpZFhKcElqb2lJbjA9OjY3YjU5YTBmYjYwYjk3ZGRiMTBhOWRhYWI3YWFlNGM0YmIyOTc0ZWNjNjY0ODhkNDczNjA5ODliMmJjNjhjNTIxMWFiNGU0NGQ3M2Q1NDI2OTViZTYzZWYxNDZhYTc2ZTgyOTc4ODQ5YTdhODE2YWFlYmE2ZTZiZWM5NWFkMmU3");
        btStartKyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<String> task) {
                        if (task.isSuccessful()) {
                            startSDK(task.getResult());
                        } else {
                            startSDK("xxx");
                        }
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        startSDK("xxx");
                    }
                });
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    private void startSDK(String fcmToken) {
        String env = "prod";
        switch (rdEnvironment.getCheckedRadioButtonId()) {
            case R.id.rdDev : env = "dev"; break;
            case R.id.rdTest : env = "test"; break;
        }

        String lang = "en";
        if (rdLanguage.getCheckedRadioButtonId() == R.id.rdTH) {
            lang = "th";
        }

        T2PKycSdkConfig config = new T2PKycSdkConfig();
        config.setKycUserRef(md5(etUserRef.getText().toString())); // kycUserRef is refer to user
        config.setKycToken(etAuthorizationToken.getText().toString()); // kycToken is key for access SDK
        config.setEnvironment(env); // For test server, default is production server
        config.setLanguage(lang);
        config.setFcmToken(fcmToken);

        T2PKycSdk.INSTANCE.initializeSdk(config).startSdk(this, new Function1<T2PKycSdkResponse, Unit>() {
            @Override
            public Unit invoke(T2PKycSdkResponse response) {
                if(response.getMeta().getResponseCode() != 600){
                    Toast.makeText(
                            MainActivity.this,
                            "(${it.meta.responseCode})${it.meta.responseMessage}",
                            Toast.LENGTH_SHORT
                    ).show();
                    return null;
                }

                String jsonToString = new com.google.gson.Gson().toJson(response);
                // Success
                Toast.makeText(
                        MainActivity.this,
                        "Success",
                        Toast.LENGTH_SHORT
                ).show();

                // You get user information form T2PKycSdkResponse.Data class
                // Log.d("T2PKycSdkResponse.Data", response.getData().toString());
                // You can send kycServiceCode and kycSessionCode to your server
                // for check or retrieve user information from API SDK Server
                return null;
            }
        });
    }

    private String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String hexString = new BigInteger(1, md.digest(input.getBytes())).toString(16);
            return String.format("%1$32s", hexString).replace(" ", "0");
        } catch (NoSuchAlgorithmException e) {
            return input;
        }
    }
}
